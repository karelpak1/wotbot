import discord
from discord.ext import commands
import asyncio


class Utilities(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False)
    # @asyncio.coroutine
    async def ping(self, ctx):
        """Ping response between discord and the user."""
        # get ready for help translation
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        t1 = ctx.message.created_at
        m = await ctx.send("...")
        time = (m.created_at - t1).total_seconds() * 1000
        await ctx.send(_("Pong! It took {}ms").format(int(time)))
        await m.delete()


def setup(bot):
    bot.add_cog(Utilities(bot))
