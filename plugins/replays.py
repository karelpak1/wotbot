import discord
from discord.ext import commands

# import asyncio
import aiohttp
from tabulate import tabulate
import re
import humanize
import time
import datetime

maps = {
    "Himmelsdorf": "https://cdn.discordapp.com/attachments/414445407729352704/450008527059288084/himmelsdorf.jpg",
    "Rockfield": "https://cdn.discordapp.com/attachments/414445407729352704/450008856744034314/13_-_Rockfield.jpg",
    "Alpenstadt": "https://cdn.discordapp.com/attachments/414445407729352704/450041821171548160/lumber-domination-alpenshtadt.png__1033x1024_q85_crop_subsampling-2_upscale.jpg",
    "None": "https://user-images.githubusercontent.com/3680926/40573302-bbe77d8e-60bf-11e8-976b-f826153dd0ef.png",
    "Canyon": "https://cdn.discordapp.com/attachments/414445407729352704/450042383170666516/canyon.jpg",
    "Mines": "https://cdn.discordapp.com/attachments/414445407729352704/450042669704675329/10_-_Mines.jpg",
    "Castilla": "https://cdn.discordapp.com/attachments/414445407729352704/450042951196737566/02_-_Castilla.jpg",
    "Fort Despair": "https://cdn.discordapp.com/attachments/414445407729352704/450043274690953218/07_-_Fort_Despair.jpg",
    "Canal": "https://cdn.discordapp.com/attachments/414445407729352704/450043568447291422/15_-_Canal.jpg",
    "Naval Frontier": "https://cdn.discordapp.com/attachments/414445407729352704/450043979078041600/map-skit-flag.jpg",
    "Desert Sands": "https://cdn.discordapp.com/attachments/414445407729352704/450044257944993803/05_-_Desert_Sands.jpg",
    "Black Goldville": "https://cdn.discordapp.com/attachments/414445407729352704/450044572597223444/01_-_Black_Goldville.jpg",
    "Dead Rail": "https://cdn.discordapp.com/attachments/414445407729352704/450044817012031499/04_-_Dead_Rail.jpg",
    "Mayan Ruins": "https://cdn.discordapp.com/attachments/414445407729352704/450045074995150858/mayan_minimap1.jpg",
    "Lost Temple": "https://cdn.discordapp.com/attachments/414445407729352704/450045317375590405/08_-_Lost_Temple.jpg",
    "Dynasty's Pearl": "https://cdn.discordapp.com/attachments/414445407729352704/450046165589688320/grossberg_overall.png",
    "Falls Creek": "https://cdn.discordapp.com/attachments/414445407729352704/450046432045563906/Falls_creek_final.jpg",
    "Mirage": "https://cdn.discordapp.com/attachments/414445407729352704/450047971472703503/mirage.jpg",
    "Winter Malinovka": "https://cdn.discordapp.com/attachments/414445407729352704/450179403914280961/14_-_Winter_Malinovka.jpg",
    "Port Bay": "https://cdn.discordapp.com/attachments/414445407729352704/450180079020933120/11_-_Port_Bay.jpg",
    "Oasis Palms": "https://cdn.discordapp.com/attachments/414445407729352704/450180302867005441/12_-_Oasis_Palms.jpg",
    "Vineyards": "https://cdn.discordapp.com/attachments/414445407729352704/450231964612427780/vineyards.jpg",
    "Yamato Harbor": "https://cdn.discordapp.com/attachments/414445407729352704/450323582221025280/yamato_harbor.jpg",
    "Copperfield": "https://cdn.discordapp.com/attachments/414445407729352704/450323982546239509/03_-_Copperfield.jpg",
    "Middleburg": "https://cdn.discordapp.com/attachments/414445407729352704/450517356196921355/09_-_Middleburg.jpg",
}


# achievements_types={"601": "titleSniper", "602": "invincible", "603": "diehard", "604": "handOfDeath", "605": "armorPiercer", "606": "cadet", "607": "firstBlood", "608": "firstVictory", "609": "androidTest", "610": "mechanicEngineer", "611": "tankExpert", "612": "mechanicEngineer0", "613": "tankExpert0", "614": "mechanicEngineer1", "615": "tankExpert1", "616": "mechanicEngineer2", "617": "tankExpert2", "618": "mechanicEngineer3", "619": "tankExpert3", "620": "mechanicEngineer4", "621": "tankExpert4", "622": "mechanicEngineer5", "623": "tankExpert5", "624": "mechanicEngineer6", "625": "tankExpert6", "626": "mechanicEngineer7", "627": "tankExpert7", "401": "battleHeroes", "402": "fragsBeast", "403": "sniperSeries", "404": "maxSniperSeries", "405": "invincibleSeries", "406": "maxInvincibleSeries", "407": "diehardSeries", "408": "maxDiehardSeries", "409": "killingSeries", "410": "maxKillingSeries", "411": "piercingSeries", "412": "maxPiercingSeries", "413": "warrior", "414": "invader", "415": "sniper", "416": "defender", "417": "steelwall", "418": "supporter", "419": "scout", "420": "medalKay", "421": "medalCarius", "422": "medalKnispel", "423": "medalPoppel", "424": "medalAbrams", "425": "medalLeClerc", "426": "medalLavrinenko", "427": "medalEkins", "428": "medalWittmann", "429": "medalOrlik", "430": "medalOskin", "431": "medalHalonen", "432": "medalBurda", "433": "medalBillotte", "434": "medalKolobanov", "435": "medalFadin", "436": "raider", "437": "kamikaze", "438": "lumberjack", "439": "beasthunter", "440": "mousebane", "441": "evileye", "442": "medalRadleyWalters", "443": "medalLafayettePool", "444": "medalBrunoPietro", "445": "medalTarczay", "446": "medalPascucci", "447": "medalDumitru", "448": "markOfMastery", "449": "medalLehvaslaiho", "450": "medalNikolas", "451": "fragsSinai", "452": "sinai", "453": "heroesOfRassenay", "454": "medalBrothersInArms", "455": "medalCrucialContribution", "456": "medalDeLanglade", "457": "medalTamadaYoshio", "458": "bombardier", "459": "huntsman", "460": "alaric", "461": "sturdy", "462": "ironMan", "463": "luckyDevil", "464": "fragsPatton", "465": "pattonValley", "466": "memberBetaTest", "467": "jointVictoryCount", "468": "jointVictory", "469": "punisherCount", "470": "punisher", "471": "medalSupremacy", "472": "camper", "473": "mainGun"}

achievements_types = {
    1: u"key",
    2: u"value",
    601: u"titleSniper",
    602: u"invincible",
    603: u"diehard",
    604: u"handOfDeath",
    605: u"armorPiercer",
    606: u"cadet",
    607: u"firstBlood",
    608: u"firstVictory",
    609: u"androidTest",
    610: u"mechanicEngineer",
    611: u"tankExpert",
    612: u"mechanicEngineer0",
    613: u"tankExpert0",
    614: u"mechanicEngineer1",
    615: u"tankExpert1",
    616: u"mechanicEngineer2",
    617: u"tankExpert2",
    618: u"mechanicEngineer3",
    619: u"tankExpert3",
    620: u"mechanicEngineer4",
    621: u"tankExpert4",
    622: u"mechanicEngineer5",
    623: u"tankExpert5",
    624: u"mechanicEngineer6",
    625: u"tankExpert6",
    626: u"mechanicEngineer7",
    627: u"tankExpert7",
    401: u"battleHeroes",
    402: u"fragsBeast",
    403: u"sniperSeries",
    404: u"maxSniperSeries",
    405: u"invincibleSeries",
    406: u"maxInvincibleSeries",
    407: u"diehardSeries",
    408: u"maxDiehardSeries",
    409: u"killingSeries",
    410: u"maxKillingSeries",
    411: u"piercingSeries",
    412: u"maxPiercingSeries",
    413: u"warrior",
    414: u"invader",
    415: u"sniper",
    416: u"defender",
    417: u"steelwall",
    418: u"supporter",
    419: u"scout",
    420: u"medalKay",
    421: u"medalCarius",
    422: u"medalKnispel",
    423: u"medalPoppel",
    424: u"medalAbrams",
    425: u"medalLeClerc",
    426: u"medalLavrinenko",
    427: u"medalEkins",
    428: u"medalWittmann",
    429: u"medalOrlik",
    430: u"medalOskin",
    431: u"medalHalonen",
    432: u"medalBurda",
    433: u"medalBillotte",
    434: u"medalKolobanov",
    435: u"medalFadin",
    436: u"raider",
    437: u"kamikaze",
    438: u"lumberjack",
    439: u"beasthunter",
    440: u"mousebane",
    441: u"evileye",
    442: u"medalRadleyWalters",
    443: u"medalLafayettePool",
    444: u"medalBrunoPietro",
    445: u"medalTarczay",
    446: u"medalPascucci",
    447: u"medalDumitru",
    448: u"markOfMastery",
    449: u"medalLehvaslaiho",
    450: u"medalNikolas",
    451: u"fragsSinai",
    452: u"sinai",
    453: u"heroesOfRassenay",
    454: u"medalBrothersInArms",
    455: u"medalCrucialContribution",
    456: u"medalDeLanglade",
    457: u"medalTamadaYoshio",
    458: u"bombardier",
    459: u"huntsman",
    460: u"alaric",
    461: u"sturdy",
    462: u"ironMan",
    463: u"luckyDevil",
    464: u"fragsPatton",
    465: u"pattonValley",
    466: u"memberBetaTest",
    467: u"jointVictoryCount",
    468: u"jointVictory",
    469: u"punisherCount",
    470: u"punisher",
    471: u"medalSupremacy",
    472: u"camper",
    473: u"mainGun",
    474: u"markOfMasteryI",
    475: u"markOfMasteryII",
    476: u"markOfMasteryIII",
}


class Replays(commands.Cog):
    """Replays upload and stats display"""

    def __init__(self, bot):
        self.bot = bot

    def clean_content(self, message, title):
        """A property that returns the content in a "cleaned up"
        manner. This basically means that mentions are transformed
        into the way the client shows it. e.g. ``<#id>`` will transform
        into ``#name``.

        This will also transform @everyone and @here mentions into
        non-mentions.
        """

        transformations = {
            re.escape("<#{0.id}>".format(channel)): "#" + channel.name
            for channel in message.channel_mentions
        }

        mention_transforms = {
            re.escape("<@{0.id}>".format(member)): "@" + member.display_name
            for member in message.mentions
        }

        # add the <@!user_id> cases as well..
        second_mention_transforms = {
            re.escape("<@!{0.id}>".format(member)): "@" + member.display_name
            for member in message.mentions
        }

        transformations.update(mention_transforms)
        transformations.update(second_mention_transforms)

        if message.guild is not None:
            role_transforms = {
                re.escape("<@&{0.id}>".format(role)): "@" + role.name
                for role in message.role_mentions
            }
            transformations.update(role_transforms)

        def repl(obj):
            return transformations.get(re.escape(obj.group(0)), "")

        pattern = re.compile("|".join(transformations.keys()))
        result = pattern.sub(repl, title)

        transformations = {"@everyone": "@\u200beveryone", "@here": "@\u200bhere"}

        def repl2(obj):
            return transformations.get(obj.group(0), "")

        pattern = re.compile("|".join(transformations.keys()))
        return pattern.sub(repl2, result)

    @commands.command(
        pass_context=True, hidden=False, aliases=["replays", "replys", "reply"]
    )
    # @asyncio.coroutine
    async def replay(self, ctx, url=None, *, title=None, stop=False):
        """Upload our reply file to WotInspector server and see battle statistics"""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        tstart = time.time()
        self.bot.logger.info(("replay", url, title))
        self.bot.logger.info(("channel", ctx.message.channel.id))
        destination = ctx.message.channel
        mastery = 0

        room_types = {
            0: _("Unknown"),
            1: _("Random battle"),
            2: _("Training room"),
            4: _("Tournament"),
            7: _("Rating battle"),
            8: _("Mad Games battle"),
            None: _("No data"),
        }
        battle_results = {
            1: _("Victory"),
            0: _("Defeat"),
            -1: _("Replay is incomplete"),
            None: None,
        }

        mastery_marks = {
            4: _("Ace Tanker"),
            3: _("1st Class"),
            2: _("2nd Class"),
            1: _("3rd Class"),
            0: 0,
            None: None,
        }

        # print("1 auto upload channel",ctx.message.content,ctx.message.attachments)
        if ctx.message.attachments and stop is not True:
            # print("2 auto upload channel",ctx.message.content,ctx.message.attachments)
            for att in ctx.message.attachments:
                # print("att:",att)
                if att.url.startswith("http") and att.url.endswith("wotbreplay"):
                    command = self.bot.all_commands["replay"]
                    # await ctx.invoke(command,stop=True)
                    # await ctx.invoke(command, ctx)
                    await ctx.invoke(
                        command, url=att.url, title=ctx.message.content, stop=True
                    )
                    # await ctx.invoke(command, url=att["url"])
                    # bot.sqlite.add_command(message.server,"replay-autoupload")
            return
        if url is None:
            await destination.send(_("You must provide URL of the replay."))
            return
        if not url.startswith("http") or not url.endswith("wotbreplay"):
            await destination.send(
                _(
                    "Replay URL must be valid address on internet pointing to  wotbreplay file"
                )
            )
            return

        replay_length = "long"
        if ctx.message.guild is not None:
            if str(ctx.message.guild.id) in self.bot.ServerCfg:
                if "replay-length" in self.bot.ServerCfg[str(ctx.message.guild.id)]:
                    replay_length = self.bot.ServerCfg[str(ctx.message.guild.id)].get(
                        "replay-length", "long"
                    )

        params = {"url": url}
        # print(ctx.message)
        # print(ctx.message.content)
        # print(ctx.message.clean_content)

        if title:
            # print(title)
            replace_replay = re.compile(re.escape("?replay"), re.IGNORECASE)
            title = replace_replay.sub("", title)
            title = title.lstrip()
            title = self.clean_content(ctx.message, title)
            if title:
                params["title"] = title
                # print(title)

        if ctx.message.guild is not None:
            if str(ctx.message.guild.id) in self.bot.ServerCfg:
                if "replay-private" in self.bot.ServerCfg[str(ctx.message.guild.id)]:
                    if (
                        str(ctx.message.channel.id)
                        in self.bot.ServerCfg[str(ctx.message.guild.id)][
                            "replay-private"
                        ]
                    ):
                        params["private"] = 1

        post_url = "https://wotinspector.com/api/replay/upload"
        async with aiohttp.ClientSession(loop=self.bot.loop) as client:
            async with client.post(post_url, params=params) as r:

                if r.status != 200:
                    await destination.send(
                        "Error, WotInspector responded with {}".format(r.status)
                    )
                    return
                response = await r.json(encoding="utf-8")
                # print("response",response)

                status = response.get("status", "error")
                # print("status",status)
                if "error" in status:
                    try:
                        await destination.send(
                            _("Error, maybe wrong URL? Message: {}").format(
                                response["error"]["message"]
                            )
                        )
                    except:
                        await destination.send(_("Error, maybe wrong URL?"))
                    return

                data = response.get("data", None)
                if data is None or "None" in data:
                    await destination.send(_("Received no data, sorry"))
                    return
                summary = data.get("summary", None)
                details = summary.get("details", None)
                # print("details:",details)
                # print(summary)

                await r.text(encoding="utf-8")
                await r.release()
        region_ = None
        protagonist = summary.get("protagonist", None)
        if protagonist:
            region_ = self.bot.wg.get_region(protagonist)
            # print("region:", region_)

        embed = discord.Embed(
            title=_("WotInspector Replay server"),
            colour=discord.Colour(0x453BD3),
            url="http://wotinspector.com/en/replays/",
            description=_(
                "See my epic replay [on WotInspector]({}) or [in Wot Blitz game]({})."
            ).format(
                data.get("view_url", "http://wotinspector.com/en/replays/"),
                data.get("download_url", "http://wotinspector.com/en/replays/"),
            ),
        )
        embed.set_author(
            name="wotinspector.com",
            url="http://wotinspector.com/en/replays/",
            icon_url="https://user-images.githubusercontent.com/3680926/40573302-bbe77d8e-60bf-11e8-976b-f826153dd0ef.png",
        )
        embed.set_footer(
            text="{}".format(_("Data by WotInspector, served by \U0001f451 WotBot"))
        )

        embed.add_field(
            name="{}".format(summary.get("title", "Battle details")[:250]),
            value=_(
                "`{player_name}@{region_}` played with {vehicle} on {map_name}"
            ).format(region_=region_, **summary),
        )
        if details is not None or None not in details:
            if "short" not in replay_length:
                battle_type = "{}".format(
                    _("Battle type: {}\n").format(
                        room_types.get(summary.get("room_type", None), _("No data"))
                    )
                    if room_types.get(summary.get("room_type", None), _("No data"))
                    else ""
                )
                battle_result = "{}".format(
                    _("Battle result: {}\n").format(
                        battle_results[summary["battle_result"]]
                    )
                    if battle_results[summary["battle_result"]]
                    else ""
                )

                battle_result = "{}".format(
                    _("Battle result: {}\n").format(
                        battle_results[summary["battle_result"]]
                    )
                    if battle_results[summary["battle_result"]]
                    else ""
                )
                if summary["winner_team"] == 0:
                    battle_result = _("Battle result: Draw\n")

                mastery_mark = "{}".format(
                    _("Mastery: {}\n").format(
                        mastery_marks.get(summary["mastery_badge"]), ""
                    )
                    if mastery_marks.get(summary["mastery_badge"], "")
                    else ""
                )
                survived = "{}".format(
                    _("Survived: {}\n").format(
                        _("Yes") if details["killed_by"] == 0 else _("No")
                    )
                )
                vehicle_tier = "{}".format(
                    _("Tier: {}\n").format(summary["vehicle_tier"])
                    if summary["vehicle_tier"]
                    else ""
                )
                battle_start = "{}".format(
                    _("Battle played: {}\n").format(
                        humanize.naturaltime(
                            datetime.datetime.fromtimestamp(
                                summary["battle_start_timestamp"]
                            )
                        )
                    )
                    if summary["battle_start_timestamp"]
                    else ""
                )
                if summary["mastery_badge"]:
                    mastery = summary["mastery_badge"]

                embed.add_field(
                    name=_("Details"),
                    value=_(
                        "```{}{}{}{}{}{survived}Distance travelled: {distance_travelled}\n```\uFEFF\n"
                    ).format(
                        battle_type,
                        battle_result,
                        mastery_mark,
                        vehicle_tier,
                        battle_start,
                        survived=survived,
                        vehicle=summary["vehicle"],
                        map_name=summary["map_name"],
                        **details
                    ),
                    inline=False,
                )

                embed.add_field(
                    name=_("Efficiency"),
                    value=_(
                        "```Damage made: {damage_made}\nDamage assisted: {damage_assisted}\nDamage received: {damage_received}\nEnemies destroyed: {enemies_destroyed}\nEnemies damaged: {enemies_damaged}\nEnemies spotted: {enemies_spotted}\nCapture points earned: {base_capture_points}\nCapture points dropped: {base_defend_points}\nVictory points earned: {wp_points_earned}\nVictory points stolen: {wp_points_stolen}```\uFEFF\n"
                    ).format(**details),
                    inline=False,
                )

                if "long" in replay_length:
                    embed.add_field(
                        name=_("Shots and hits"),
                        value=_(
                            "```Shots fired: {shots_made} \nShots pen: {shots_pen}\nHits received: {hits_received}\nHits pen: {hits_pen}\nHits bounced: {hits_bounced}```\uFEFF\n"
                        ).format(**details),
                        inline=False,
                    )
                    embed.add_field(
                        name=_("Experience and Credits"),
                        value=_(
                            "```Base experience: {exp}\nTotal experience: {exp_total}\nBase credits: {credits_base}\nTotal credits: {credits_total}```\uFEFF\n"
                        ).format(**summary, **details),
                        inline=False,
                    )

                    achievements = details.get("achievements", None)
                    if achievements:
                        # print(achievements)
                        # achievements_wg=dict(self.bot.wg.wotb_servers["eu"].encyclopedia.achievements())
                        achievements_wg = await self.bot.wg.get_all_achievements()
                        achievs_all = []
                        for achiev in achievements:
                            series = ""
                            achiev_name = achievements_types.get(achiev["t"], None)
                            if achiev_name:
                                wg_name = achievements_wg.get(achiev_name, None)
                                if not wg_name:
                                    wg_name = achievements_wg.get(
                                        achiev_name.replace("Series", ""), None
                                    )
                                    if wg_name:
                                        series = "Series"

                                if wg_name:
                                    name = wg_name["name"]
                                else:
                                    name = achiev_name
                                # if "markOfMastery" in achiev_name:
                                #    achievs_all.append("{}: {}".format(name,mastery_marks.get(achiev["v"],"")))
                                # else:
                                # achievs_all.append("{}{}: {}".format(name,series,achiev["v"]))
                                achievs_all.append("{} {}".format(name, series))
                        embed.add_field(
                            name=_("Medals and Achievements"),
                            value="```{}```\uFEFF\n".format("\n".join(achievs_all)),
                            inline=False,
                        )

                    allies = []
                    a_ttl_wr = 0
                    a_ttl_dpb = 0
                    a_ttl_battles = 0
                    # for player_id in summary.get("allies",None):
                    players = ",".join(str(x) for x in summary.get("allies", None))
                    # players,region=self.bot.wg.get_players_by_ids(players,region_)

                    players, region = await self.bot.wg.get_players_by_ids_a(
                        players, region_
                    )

                    for player_id, player in players.items():
                        # player=player[player_id]
                        # print(player)

                        if player:
                            # print(player)
                            player_id = str(player["account_id"])
                            player_nick = player["nickname"]
                            battles = player["statistics"]["all"]["battles"]
                            battles = player["statistics"]["all"]["battles"]
                            wins = player["statistics"]["all"]["wins"]
                            damage_dealt = player["statistics"]["all"]["damage_dealt"]
                            try:
                                wr = round(wins / float(battles) * 100, 2)
                            except:
                                wr = 0
                            try:
                                dpb = round(damage_dealt / float(battles), 2)
                            except:
                                dpb = 0

                            allies.append([player_nick[:14], wr, dpb, battles])

                            a_ttl_dpb += dpb
                            a_ttl_wr += wr
                            a_ttl_battles += battles
                    ta = sorted(allies, key=lambda x: (x[0]))
                    ta.append(
                        [
                            _("Average:"),
                            round(a_ttl_wr / len(summary.get("allies", None)), 2),
                            round(a_ttl_dpb / len(summary.get("allies", None)), 2),
                            round(a_ttl_battles / len(summary.get("allies", None)), 2),
                        ]
                    )

                    embed.add_field(
                        name=_("Allies"),
                        value="```{}```\uFEFF\n".format(
                            tabulate(
                                ta,
                                headers=[_("Name"), _("WR"), _("DPB"), _("Battles")],
                                stralign="left",
                                floatfmt=".2f",
                                numalign="right",
                            )
                        ),
                        inline=False,
                    )

                    enemies = []
                    e_ttl_wr = 0
                    e_ttl_dpb = 0
                    e_ttl_battles = 0
                    players = ",".join(str(x) for x in summary.get("enemies", None))
                    if players:
                        # players,region=self.bot.wg.get_players_by_ids(players,region_)
                        players, region = await self.bot.wg.get_players_by_ids_a(
                            players, region_
                        )
                        # print("returned",players)
                        for player_id, player in players.items():
                            # player=player[player_id]
                            # print(player)

                            if player:
                                # print(player)
                                player_id = str(player["account_id"])
                                player_nick = player["nickname"]
                                battles = player["statistics"]["all"]["battles"]
                                wins = player["statistics"]["all"]["wins"]
                                damage_dealt = player["statistics"]["all"][
                                    "damage_dealt"
                                ]
                                try:
                                    wr = round(wins / float(battles) * 100, 2)
                                except:
                                    wr = 0
                                try:
                                    dpb = round(damage_dealt / float(battles), 2)
                                except:
                                    dpb = 0

                                enemies.append([player_nick[:14], wr, dpb, battles])
                                e_ttl_dpb += dpb
                                e_ttl_wr += wr
                                e_ttl_battles += battles
                        te = sorted(enemies, key=lambda x: (x[0]))
                        te.append(
                            [
                                _("Average:"),
                                round(e_ttl_wr / len(summary.get("enemies", None)), 2),
                                round(e_ttl_dpb / len(summary.get("enemies", None)), 2),
                                round(
                                    e_ttl_battles / len(summary.get("enemies", None)), 2
                                ),
                            ]
                        )
                        embed.add_field(
                            name=_("Enemies"),
                            value="```{}```\uFEFF\n".format(
                                tabulate(
                                    te,
                                    headers=[
                                        _("Name"),
                                        _("WR"),
                                        _("DPB"),
                                        _("Battles"),
                                    ],
                                    stralign="left",
                                    floatfmt=".2f",
                                    numalign="right",
                                )
                            ),
                            inline=False,
                        )
        else:
            embed.add_field(
                name=_("Game details"),
                value=_(
                    "No detailed game data in replay file. Always wait for battle end to have complete capture."
                ),
            )
        if mastery > 0:
            embed.set_thumbnail(
                url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/big/markOfMastery{}.png".format(
                    mastery
                )
            )
        else:
            embed.set_thumbnail(
                url="{}".format(
                    maps.get(
                        summary.get("map_name", "None"),
                        "https://user-images.githubusercontent.com/3680926/40573302-bbe77d8e-60bf-11e8-976b-f826153dd0ef.png",
                    )
                )
            )
        # await self.bot.send_message(destination,content=None, embed=embed)

        try:
            await destination.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            try:
                await destination.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
            except discord.Forbidden:
                self.bot.logger.warning("Cannot send any message!!!")
        print(humanize.naturaldelta(tstart - time.time()))


def setup(bot):
    bot.add_cog(Replays(bot))
