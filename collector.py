# aiohttp rate limiting by gist:
# https://gist.github.com/pquentin/5d8f5408cdad73e589d85ba509091741

import aiosqlite as lite
import json
import datetime
import humanize
import asyncio
import time
import aiohttp
import sys
from pathlib import Path
import os.path
import os
import gzip
import zlib

import time
import shutil
import subprocess


os.chdir(os.path.dirname(os.path.abspath(__file__)))
log_file = "collector.log"


def backup_file():
    for i in range(1, 5):
        out = subprocess.call(
            ["sqlite3", "data/users.db", ".backup data/users_backup.db"], shell=False
        )
        my_file = Path("data/users_backup.db")
        if my_file.is_file():
            if my_file.stat().st_size > 1024:
                shutil.move("data/users_backup.db", "data/users_copy.db")
                return
            else:
                print(i, "bad try to get database copy, sleep for", 2 * i)
                time.sleep(2 * i)
    print("using old database")


# here we make a copy of the live database
backup_file()
with open("data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

token = cfg["WargamingCollectorToken"]
privatetoken = cfg["WargamingPrivateToken"]


class DB:
    def __init__(self, region="eu"):
        # self.con = lite.connect('data/users.db', detect_types=lite.PARSE_DECLTYPES)
        # self.con = lite.connect('file:data/users_copy.db?mode=ro', timeout=1000,uri=True, detect_types=lite.PARSE_DECLTYPES)
        self.region = region

    async def remove_old_users_from_daily(self):
        # 3 days is enough as this is only updated by data from users, who are erased after 30 days
        w = dict(since=(datetime.datetime.utcnow() - datetime.timedelta(days=3)))

        async with lite.connect(
            "file:data/daily.db",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            # db._conn.row_factory = lite.core.sqlite3.Row
            await db.execute("delete from daily where today_date < $since;", w)
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute("SELECT changes()") as c:
                deleted = await c.fetchone()
            await db.commit()
            return deleted[0]

    async def remove_old_users_from_users(self):
        # 30 days of inactivity
        w = dict(since=(datetime.datetime.utcnow() - datetime.timedelta(days=30)))

        async with lite.connect(
            "file:data/users.db",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            # db._conn.row_factory = lite.core.sqlite3.Row
            await db.execute("delete from users where time_stamp < $since;", w)
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute("SELECT changes()") as c:
                deleted = await c.fetchone()
            await db.commit()
            return deleted[0]

    async def get_old_user(self):
        w = dict(since=(datetime.datetime.utcnow() - datetime.timedelta(days=30)))

        async with lite.connect(
            "file:data/users.db?mode=ro",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "select account_id from users where time_stamp < $since;", w
            ) as cursor:
                rows = await cursor.fetchall()
                return rows

    async def erase_token_user(self, account_id):
        w = {}
        w["account_id"] = account_id

        async with lite.connect(
            "file:data/users.db",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            await db.execute(
                "update users set token = null where account_id =$account_id", w
            )
            await db.commit()

    async def get_user(self, private=None):
        w = dict(since=(datetime.datetime.utcnow() - datetime.timedelta(days=30)))
        w["region"] = self.region

        async with lite.connect(
            "file:data/users.db?mode=ro",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            if not private:
                async with db.execute(
                    "select account_id from users where region == $region and time_stamp > $since;",
                    w,
                ) as cursor:
                    rows = await cursor.fetchall()
                    return rows
            else:
                async with db.execute(
                    "select account_id,token from users where region == $region and time_stamp > $since and token is not null;",
                    w,
                ) as cursor:
                    rows = await cursor.fetchall()
                    return rows

    async def store_daily(self, user, data, what, private=None):
        w = {}
        w["user"] = user
        # w["data"]=json.dumps(data).encode('utf-8')
        w["data"] = zlib.compress(json.dumps(data).encode("utf-8"))
        w["date"] = datetime.datetime.utcnow()
        today_what = "today_{}".format(what)
        yesterday_what = "yesterday_{}".format(what)

        async with lite.connect(
            "file:data/daily.db",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            # async with db.execute("select account_id, today from daily where region == $region and time_stamp > $since;",w) as cursor:
            await db.execute("insert or ignore into daily(account_id) values($user)", w)
            if not private:
                if today_what == "today_account_info":  # move date only once
                    await db.execute(
                        "update daily set {yesterday_what} = {today_what}, {today_what}=$data, yesterday_date=today_date,today_date=$date where account_id =$user".format(
                            yesterday_what=yesterday_what, today_what=today_what
                        ),
                        w,
                    )
                else:
                    await db.execute(
                        "update daily set {yesterday_what} = {today_what}, {today_what}=$data,today_date=$date where account_id =$user".format(
                            yesterday_what=yesterday_what, today_what=today_what
                        ),
                        w,
                    )
            else:
                await db.execute(
                    "update daily set {today_what}=$data, today_date=$date where account_id =$user".format(
                        today_what=today_what
                    ),
                    w,
                )

            await db.commit()


class RateLimiter:
    """Rate limits an HTTP client that would make get() and post() caAAlls.

    Calls are rate-limited by host.
    https://quentin.pradet.me/blog/how-do-you-rate-limit-calls-with-aiohttp.html
    This class is not thread-safe."""

    RATE = 20
    MAX_TOKENS = 20

    def __init__(self, client):
        self.client = client
        self.tokens = self.MAX_TOKENS
        self.updated_at = time.monotonic()

    async def get(self, *args, **kwargs):
        await self.wait_for_token()
        return self.client.get(*args, **kwargs)

    async def wait_for_token(self):
        while self.tokens < 1:
            self.add_new_tokens()
            await asyncio.sleep(1)
        self.tokens -= 1

    def add_new_tokens(self):
        now = time.monotonic()
        time_since_update = now - self.updated_at
        new_tokens = time_since_update * self.RATE
        if new_tokens > 1:
            self.tokens = min(self.tokens + new_tokens, self.MAX_TOKENS)
            self.updated_at = now


async def main(db):
    # u = await db.get_old_user()
    # count = 0
    # print("old records", len(u))
    # for i in u:
    #    # print("old",i["account_id"])
    #    for p in Path("stats/daily/").glob("{}*".format(i["account_id"])):
    #        # print(p)
    #        p.unlink()
    #        count += 1
    #        # print("removed")
    count_users = await db.remove_old_users_from_users()
    count_daily = await db.remove_old_users_from_daily()

    with open(log_file, "a") as the_file:
        the_file.write("Removed {} users records\n".format(count_users))
        the_file.write("Removed {} daily records\n".format(count_daily))

    connector=aiohttp.TCPConnector(ssl=False, loop = loop)
    async with aiohttp.ClientSession(connector=connector) as client:
    #async with aiohttp.ClientSession(loop=loop) as client:
        client = RateLimiter(client)

        async def get_files(url, file_name):
            for i in range(3):
                async with await client.get(url) as resp:
                    if resp.status != 200:
                        await resp.release()
                        print("retry on status {}".format(resp.status), i)
                        continue
                    data = await resp.json()
                    if data.get("status") == "ok":
                        for acc_id, acc_data in data["data"].items():
                            await db.store_daily(acc_id, acc_data, file_name)
                            # filename = "stats/daily/{}-{}.txt".format(acc_id, file_name)
                            # with gzip.open(filename, "w") as f:
                            #    f.write(json.dumps(acc_data).encode("utf-8"))
                        return
                    elif data.get("status") == "error":
                        await resp.release()
                        if data["error"]["code"] == 504:
                            print("retry on err 504", i)
                        else:
                            return

        async def get_single_file(url, file_name, acc_id, private=None):
            for i in range(3):
                async with await client.get(url) as resp:
                    if resp.status != 200:
                        await resp.release()
                        print("retry on status {}".format(resp.status), i)
                        continue
                    data = await resp.json()
                    if data.get("status") == "ok":
                        if private:
                            data = data["data"][str(acc_id)]
                            await db.store_daily(
                                acc_id, data, file_name, private=private
                            )
                            return
                        else:
                            await db.store_daily(
                                acc_id, data, file_name, private=private
                            )
                            return
                    elif data.get("status") == "error":
                        await resp.release()
                        if data["error"]["code"] == 504:
                            print("retry on err 504", i)
                        else:
                            print(data["error"]["message"], acc_id)
                            if "INVALID_ACCESS_TOKEN" in data["error"]["message"]:
                                await db.erase_token_user(acc_id)
                            return

        u = await db.get_user()
        hundred = []
        for i in u:
            # print(i["account_id"])
            hundred.append(i["account_id"])

            if len(hundred) > 99:
                ids = ",".join(str(x) for x in hundred)
                hundred = []

                url = "https://api.wotblitz.{}/wotb/account/info/?application_id={}&account_id={}&extra=statistics.rating".format(
                    db.region.replace("na", "com"), token, ids
                )
                filename = "account_info"
                try:
                    await get_files(url, filename)
                except Exception as e:
                    print(e)

                url = "https://api.wotblitz.{}/wotb/account/achievements/?application_id={}&account_id={}".format(
                    db.region.replace("na", "com"), token, ids
                )
                filename = "achievements"
                try:
                    await get_files(url, filename)
                except Exception as e:
                    print(e)

        if hundred:  # leftover
            ids = ",".join(str(x) for x in hundred)
            hundred = []

            url = "https://api.wotblitz.{}/wotb/account/info/?application_id={}&account_id={}&extra=statistics.rating".format(
                db.region.replace("na", "com"), token, ids
            )
            filename = "account_info"
            try:
                await get_files(url, filename)
            except Exception as e:
                print(e)

            url = "https://api.wotblitz.{}/wotb/account/achievements/?application_id={}&account_id={}".format(
                db.region.replace("na", "com"), token, ids
            )
            filename = "achievements"
            try:
                await get_files(url, filename)
            except Exception as e:
                print(e)

        # u=await db.get_user()
        count = 0
        for i in u:
            # print(i["account_id"])
            count += 1

            url = "https://api.wotblitz.{}/wotb/tanks/stats/?application_id={}&account_id={}".format(
                db.region.replace("na", "com"), token, i["account_id"]
            )
            filename = "tank_stats"
            try:
                await get_single_file(url, filename, i["account_id"])
            except Exception as e:
                print(e)

            url = "https://api.wotblitz.{}/wotb/tanks/achievements/?application_id={}&account_id={}".format(
                db.region.replace("na", "com"), token, i["account_id"]
            )
            filename = "vehicle_achievements"
            try:
                await get_single_file(url, filename, i["account_id"])
            except Exception as e:
                print(e)

        u = await db.get_user(private=True)
        count_p = 0

        for i in u:

            count_p += 1
            url = "https://api.wotblitz.{}/wotb/account/info/?application_id={}&account_id={}&access_token={}&extra=statistics.rating&fields=statistics.all,statistics.rating,private".format(
                db.region.replace("na", "com"),
                privatetoken,
                i["account_id"],
                i["token"],
            )
            filename = "account_info"
            try:
                await get_single_file(url, filename, i["account_id"], private=True)
                pass
            except Exception as e:
                print(e)

        with open(log_file, "a") as the_file:
            the_file.write(
                "Fetched {} accounts, {} private for {}\n".format(
                    count, count_p, db.region
                )
            )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    if len(sys.argv) < 2:
        print("need parameter: region")
        sys.exit(0)
    db = DB(sys.argv[1])
    with open(log_file, "a") as the_file:
        the_file.write(
            "{}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        )

    loop.run_until_complete(main(db))

    with open(log_file, "a") as the_file:
        the_file.write(
            "{}\n\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        )
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.
