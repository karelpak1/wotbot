import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math


# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=True, aliases=["quiz", "blitzquiz"])
    # @asyncio.coroutine
    async def trivia(self, ctx):
        """trivia"""
        autorun = False
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        embed = discord.Embed(
            title="test", description="```{}```".format("test"), colour=234, type="rich"
        )
        embed.set_footer(text="{}".format("test"))
        # embed.set_thumbnail(url=percents[round(wr_ttl, -1)/10])
        try:
            msg = await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(content="Please enable Embed links permission for wotbot.")
        try:
            await self.bot.add_reaction(
                msg, calc_emoji
            )  # test of votable emoji → "reaction"
            await self.bot.add_reaction(
                msg, reload_emoji
            )  # test of votable emoji → "reaction"
        except discord.Forbidden:
            self.bot.logger.warning(
                "Please enable Add reactions permission for wotbot."
            )
            await ctx.send(content="Please enable Add reactions permission for WotBot.")

        await asyncio.sleep(0.1)
        while True:
            res = await self.bot.wait_for_reaction(
                [reload_emoji, calc_emoji], message=msg, user=ctx.message.author
            )
            # self.bot.logger.debug(res.reaction.message.author)
            # self.bot.logger.debug(res.user)
            if res.reaction.emoji == reload_emoji:
                print("aaa")
            elif res.reaction.emoji == calc_emoji:
                print("bbb")

        print("exit?")


def setup(bot):
    bot.add_cog(UserStats(bot))
