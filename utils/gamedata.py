from pathlib import Path
import difflib
import xml.etree.ElementTree
from pprint import pprint
import sys
from random import randint
import json
import csv

# param=sys.argv[1]
class Data:
    def __init__(self):
        with open("./game_data/latest/gamedata.json") as game_data:
            self.data = json.load(game_data)
            self.keys = list(self.data.keys())

    def hledej(self, param):
        factor = 0.6
        while True:
            res = difflib.get_close_matches(param.lower(), self.keys, 3, factor)
            # print("Found:", res)
            # print("Factor:", factor)
            if not res:
                factor = factor - 0.1
            else:
                break

        if not res:
            return None

        result = res[0]

        return self.data[result]

    def quiz(self):
        while True:
            tank = self.keys[randint(0, len(self.keys) - 1)]
            if tank not in [
                "list",
                "customization",
                "chassis",
                "engines",
                "fuelTanks",
                "guns",
                "radios",
                "shells",
                "turrets",
                "vehicle",
                "vehicle_effects",
                "stuff_packages",
                "stuff",
                "shot_effects",
                "provisions",
                "player_emblems",
                "optional_devices",
                "optional_device_slots",
                "horns",
                "gun_effects",
                "damage_stickers",
                "consumables",
                "chassis_effects",
                "camouflages",
            ]:
                break

        result = self.hledej(tank)
        return result
