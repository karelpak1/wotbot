import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
import aiohttp


# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=True)
    # @asyncio.coroutine
    async def savestats(self, ctx, *, player_name: str = None):
        """Make a checkpoint of tier stats."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)
        color_stats = "json"

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier")
        all_vehicles = await self.bot.wg.get_all_vehicles()

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("savestats:", player_name, region))

        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]

            all_tiers = {}
            result_total = Counter()
            print_tiers = []
            # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=player_id, fields="tank_id, all.wins, all.battles,all.damage_dealt")[player_id]
            player_tanks = await self.bot.wg.get_wg(
                region=region,
                cmd_group="tanks",
                cmd_path="stats",
                parameter="account_id={}&fields=tank_id, all.wins, all.battles,all.damage_dealt".format(
                    player_id
                ),
            )
            player_tanks = player_tanks[player_id]
            #print(player_tanks)
            if player_tanks:
                for tank in player_tanks:
                    if str(tank["tank_id"]) in all_vehicles:
                        tier = all_vehicles[str(tank["tank_id"])]["tier"]
                    else:
                        tier = 0
                    if tier not in all_tiers:
                        all_tiers[tier] = tank["all"]
                    else:
                        temp = dict(Counter(all_tiers[tier]) + Counter(tank["all"]))
                        all_tiers[tier] = temp
                    result_total.update(tank["all"])

            self.bot.logger.debug(all_tiers)
            self.bot.logger.debug(result_total)

            player_rating, player_rating_league_icon, player_rating_league = await self.bot.wg.get_rating(
                player_id, region, ctx, score=1
            )

            self.bot.logger.debug(all_tiers)
            for i in all_tiers:
                print_tiers.append(
                    [
                        i,
                        all_tiers[i]["battles"],
                        all_tiers[i]["wins"] / all_tiers[i]["battles"] * 100,
                        all_tiers[i]["damage_dealt"] / all_tiers[i]["battles"],
                    ]
                )
            #print(all_tiers)
            with open(
                "./stats/{}-{}.pickle".format(ctx.message.author.id, player_id), "wb"
            ) as storage:
                pickle.dump(
                    [datetime.datetime.now(), all_tiers, {"rating": player_rating}],
                    storage,
                )

            print_tiers = sorted(print_tiers, key=lambda x: (x[0], x[3]))
            try:
                print_tiers.append(
                    [
                        _("Total:"),
                        result_total["battles"],
                        result_total["wins"] / result_total["battles"] * 100,
                        result_total["damage_dealt"] / result_total["battles"],
                    ]
                )
            except ArithmeticError as e:
                self.bot.logger.info(e)
            toutotal = _("{tiers}\nRating: {rating}\n").format(
                tiers=tabulate(
                    print_tiers,
                    headers=[_("Tier"), _("Battles"), _("WR"), _("DMG")],
                    floatfmt=".2f",
                    numalign="right",
                ),
                rating=player_rating,
            )

            embed = discord.Embed(
                title=_(
                    "Saving data for `{0}@{1}`. Get tier stats diff by using ?readstats `{0}@{1}`"
                ).format(player_nick, region),
                description="```{color}\n{out}```".format(
                    color=color_stats, out=toutotal
                ),
                colour=234,
                type="rich",
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            # embed.add_field(name='```Winrate:``` `{}`'.format(data["period30d"]["all"]["battles"]),value='\uFEFF')
            embed.set_footer(
                text=_("After some battles, check stats by clicking {}").format(
                    calc_emoji
                )
            )

            if player_rating_league_icon is not None:
                embed.set_thumbnail(url=player_rating_league_icon)
            else:
                embed.set_thumbnail(
                    url="https://wotblitz.com/newstatic/images/twister_icon.png"
                )
            try:
                msg = await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    _("Please enable Embed links permission for wotbot.")
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
            try:
                await msg.add_reaction(calc_emoji)  # test of votable emoji → "reaction"
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Add reactions permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Add reactions permission for WotBot.")
                )

            def check(reaction, user):
                # self.bot.logger.debug((user,ctx.message.author,msg.author))
                # print("checking wstats",user,ctx.message.author,msg.author)
                if msg.id == reaction.message.id:
                    if user != msg.author:
                        return reaction, user

            await asyncio.sleep(0.1)
            # while True:
            # res=await self.bot.wait_for_reaction(calc_emoji, message=msg,check=check)
            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", check=check, timeout=300
                )
            except asyncio.TimeoutError:
                print("timed out")
                try:
                    await msg.remove_reaction(calc_emoji, msg.author)
                except Exception as e:
                    self.bot.logger.info(e)
                return
            print(user, ctx.message.author)
            # if reaction.emoji==calc_emoji and user==ctx.message.author:
            if reaction.emoji == calc_emoji:
                # self.bot.remove_reaction(msg,'\N{THUMBS UP SIGN}',msg.author)
                # await self.bot.delete_message(msg)
                command = self.bot.all_commands["readstats"]
                await ctx.invoke(command, player_name=player_nick)
            #    break
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(UserStats(bot))
