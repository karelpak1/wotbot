import discord
from discord.ext import commands
from discord.ext.commands import bot as bot_module
import asyncio
import inspect


class Utilities(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        name="help",
        aliases=["wotbot", "hello", "command", "commands"],
    )
    # @asyncio.coroutine
    async def help(self, ctx, *commands_):
        """Shows help message."""
        self.bot.logger.info("help")

        # get ready for translations. must fogure out command __doc__ switching for help...
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext

        #destination = ctx.message.author
        destination = ctx.message.channel
        # if self.bot.pm_help else ctx.message.channel
        #destination_quick_help = (
        #    ctx.message.author if self.bot.pm_help else ctx.message.channel
        #)

        # help by itself just lists our own commands.
        # fast hekp page first:
        prefix = await self.bot.get_prefix(ctx.message)
        embed = discord.Embed(
            title=_("Most useful WotBot commands"),
            description=_(
                """:stopwatch:**{0}daily** *player* - today's stats
:calendar:**{0}readstats** *player* - stats for time period
:tropical_drink:**{0}unicum** *player* - carrier stats and goals
:scales:**{0}winrate** *player* - quick carrier stats
:popcorn:**{0}denoob** *player* - detailed per tier carrier stats and goals

:first_place:**{0}mastery** *player* - list of mastered tanks
:chart_with_upwards_trend:**{0}bsig** *player* - BlitzStars signature
:play_pause:**{0}replay url** - put replay to WotInspector, get details
:game_die:**{0}quiz** - play tank quiz game

:family_wwbb:**{0}clanstats** *clan* - clan stats
There are more useful clan commands, see them [here](https://wotbot.pythonanywhere.com/help#ClanStats)

:homes:**{0}invite** - invite WotBot to your server
:tools:**{0}conf** - set your game name, region and other
"""
            ).format(prefix[0]),
            colour=234,
            type="rich",
            url="https://wotbot.pythonanywhere.com/help",
        )
        # embed.set_author(name="b48g55m", url="https://github.com/vanous/", icon_url="http://www.newdesignfile.com/postpic/2015/04/android-icon_321698.png")
        embed.set_footer(
            text="WotBot help",
            icon_url="https://cdn.discordapp.com/attachments/402196519991902218/764852729486901298/wotbot_02.png",
        )
        # for i in sorted(categ_list):
        # embed.add_field(name='Most used commands', value=":question:**?help [command]** - See this help or help for a command\n2. bbb\n3. ccc\4. ddd",inline=False)
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png"
        )

        try:
            await destination.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning(
                "Please enable Embed links permission for wotbot."
            )
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )

        return


def setup(bot):
    bot.add_cog(Utilities(bot))
