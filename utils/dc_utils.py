import discord


class DC:
    def __init__(self, bot):
        self.bot = bot

    async def typing(self, ctx):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        try:
            await ctx.trigger_typing()
        except discord.Forbidden:
            self.bot.logger.info("Cannot send any message to channel, contacting user")
            await ctx.message.author.send(
                content=_(
                    "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                )
            )
            if ctx.message.guild is not None:
                self.bot.logger.warning(
                    (ctx.message.guild.name, ctx.message.author.name)
                )
            else:
                self.bot.logger.warning((ctx.message.author.name))

    async def not_found_msg(self, ctx, out):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        try:
            await ctx.message.add_reaction("\U0001f4e8")
        except discord.Forbidden:
            pass
        await ctx.message.author.send(content=out)

    async def contributor(self, ctx, player, region):
        out = ""
        acc_id = player[0]["account_id"]
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        emojis = {
            "cz": "\U0001f1e8\U0001f1ff",
            "fr": "\U0001f1eb\U0001f1f7",
            "hu": "\U0001f1ed\U0001f1fa",
            "de": "\U0001f1e9\U0001f1ea",
            "nl": "\U0001f1f3\U0001f1f1",
            "ru": "\U0001f1f7\U0001f1fa",
            "pl": "\U0001f1f5\U0001f1f1",
            "it": "\U0001f1ee\U0001f1f9",
            "es": "\U0001f1ea\U0001f1f8",
            "mage": "\U0001f451",
            "ar": "\U0001f1f8\U0001f1e6",
            "heart": "\U00002764",
            "zh":"\U0001f1e8\U0001f1f3",
        }
        contributors = {
            546103095: ["translator", "fr"],  # Myrheii
            531602441: ["author", "mage"],  # b48g55m
            534883764: ["supporter", "heart"],  # HappyJuice_CZE
            6417251: ["andrew", "ru"],  # reven86
            508273333: ["translator", "fr"],  # fuzzripper
            524843222: ["translator", "de"],  # andreas_kober
            559429393: ["translator", "hu"],  # bhfc
            531502052: ["supporter", "heart"],  # betakar97
            21968473: ["translator", "ru"],  # vdo2000
            1841696: ["translator", "ru"],  # FanHamMer
            514588527: ["translator", "nl"],  # sjonscom
            21968473: ["translator", "ru"],  # vdo2000
            540530639: ["translator", "pl"],  #Banquo__
            522536142: ["translator", "it"],  #shocky
            560416041:["translator","es"], 
            541998411:["translator","ar"], 
            323185120255148041:["translator","tr"],
            555671839993757706:["translator","zh"],
        }
        roles = {
            "author": _("WotBot author. {emoji}"),
            "translator": _("WotBot translator. {emoji}"),
            "supporter": _("WotBot supporter. {emoji}"),
            "andrew": _("WotBot translator and WotInspector author. {emoji}"),
        }

        if acc_id in contributors:
            contributor = contributors[acc_id]
            out = roles[contributor[0]].format(emoji=emojis[contributor[1]])

        return out

    async def confirmed(self, ctx, ownuser):
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        if ownuser:
            return _("WG account confirmed. {emoji}").format(emoji="\U0001f44d")
        else:
            return _("Unconfirmed WG account {emoji} you can confirm it here.").format(
                emoji="\U0001f464"
            )

    async def confirmed_url(self, ctx, ownuser):
        if ownuser:
            return ""
        else:
            return "https://wotbot.pythonanywhere.com/"
