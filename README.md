# wotbot

This is WoT Blitz Bot for Discord.

Invite: https://discordapp.com/oauth2/authorize?client_id=368866137603309569&scope=bot&permissions=0

WotBot Dashboard: http://wotbot.pythonanywhere.com/

List of commands: http://wotbot.pythonanywhere.com/help

Development server and community: https://discord.gg/mzXYPVW

Forum: http://forum.wotblitz.eu/index.php?/topic/45862-discord-bot-wotbot/

Licensed under GNU Affero General Public License version 3

