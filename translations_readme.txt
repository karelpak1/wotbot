#extract string
pybabel extract -o messages.pot .

#new language, run only once!
pybabel init -i messages.pot -d translations -l cz_CZ

#after string (re) extraction, update strings
pybabel update -i messages.pot -d translations

#after traslation, upload ne po files and recompile
pybabel compile -d translations


add a language:
* add a language on Crowdin
    * pull langs from crowdin
* add lang load to wotbot.py
    * start_lang
* add lang to config.py
    * lang
    * playerlanguage
* add player to dc_utils.py
    * contributors
* add lang to dashboard

