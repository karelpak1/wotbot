import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

import humanize
import time

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
import aiohttp

tabulate.PRESERVE_WHITESPACE = True

# unicode list here: http://www.unicode.org/charts/PDF/U1F300.pdf

reload_emoji = "\U0001F512"
calc_emoji = "\U0001F522"


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # @commands.cooldown(rate=1, per=2000, type=commands.BucketType.guild)
    @commands.command(
        pass_context=True, hidden=False, aliases=["g-wr", "gwr", "guildwr"]
    )
    async def guildwinrate(self, ctx):
        """Guild winrate"""
        tstart = time.time()
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier")

        # all_vehicles = await self.bot.wg.get_all_vehicles()
        guild_members = {}

        if ctx.message.guild is not None:
            for member in ctx.message.guild.members:
                # print(member.name)
                wg_confirmed = True
                wg_name = await self.bot.wg.get_confirmed_name(member.id)
                if not wg_name:
                    wg_confirmed = False
                    wg_name = await self.bot.wg.get_local_name_by_id(member.id)
                if wg_name:
                    # print(member.id, member.name, wg_name)
                    guild_members[member.id] = {
                        "name": member.name,
                        "wg_name": wg_name,
                        "confirmed": wg_confirmed,
                        "wg_stats": {"random": {}, "rating": {}},
                    }

        else:
            await ctx.send(content=_("Must run in a channel, not in Direct message"))

        print(
            "total in guild: {}/{}".format(
                len(ctx.message.guild.members), len(guild_members)
            )
        )

        if len(guild_members) > 15:
            await ctx.send(
                content=_(
                    "This will take some time to calculate, there are {} players to check.".format(
                        len(guild_members)
                    )
                )
            )

        self.bot.logger.info(("guild winrate:", ctx.message.guild.name))
        for gmember, gvalues in guild_members.items():
            print("try: '{}' '{}'".format(gvalues["name"], gvalues["wg_name"]))
            player_id = False
            if gvalues["confirmed"]:
                player_nick = gvalues["wg_name"]["name"]
                player_id = gvalues["wg_name"]["account_id"]
                region = gvalues["wg_name"]["region"]
            else:
                player, region = await self.bot.wg.get_player_a(
                    gvalues["wg_name"], ctx.message
                )
                print(player, region)
                if player and region:
                    player_id = str(player[0]["account_id"])
                    player_nick = player[0]["nickname"]

            if not player_id:
                print("skip", gmember)
                continue

            info_data = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}&extra=statistics.rating".format(player_id),
            )

            if info_data:
                last_battle = datetime.datetime.fromtimestamp(
                    info_data[player_id].get("last_battle_time", None)
                )
                created_at = datetime.datetime.fromtimestamp(
                    info_data[player_id].get("created_at", None)
                )

                info_statistics = info_data[player_id].get("statistics", None)
            if info_statistics:
                all_data = {}
                info_all = info_statistics.get("all", None)
                all_data["battles"] = info_all.get("battles", False)
                try:
                    all_data["dpb"] = round(
                        info_all["damage_dealt"] / float(info_all["battles"]), 2
                    )
                except ArithmeticError as e:
                    all_data["dpb"] = False
                try:
                    all_data["winrate"] = round(
                        info_all["wins"] / float(info_all["battles"]) * 100, 2
                    )
                except ArithmeticError as e:
                    all_data["winrate"] = False

                rating_data = {}
                info_rating = info_statistics.get("rating", None)
                rating_data["battles"] = info_rating.get("battles", False)
                try:
                    rating_data["dpb"] = round(
                        info_rating["damage_dealt"] / float(info_rating["battles"]), 2
                    )
                except ArithmeticError as e:
                    rating_data["dpb"] = False
                try:
                    rating_data["winrate"] = round(
                        info_rating["wins"] / float(info_rating["battles"]) * 100, 2
                    )
                except ArithmeticError as e:
                    rating_data["winrate"] = False
                gvalues["wg_stats"]["random"] = all_data
                gvalues["wg_stats"]["rating"] = rating_data
                gvalues["wg_stats"]["random"]["last"] = last_battle
                gvalues["wg_stats"]["random"]["created"] = created_at

        # print(guild_members)

        # headers=[_("Tier"), _("Battles"), _("WR"), _("DMG")],

        embed = discord.Embed(
            title="placeholder", description="aa", colour=234, type="rich"
        )

        embed.set_footer(
            text=_(
                "Results cannot be switched in less then 10 seconds. Calculation has taken {time}".format(
                    time=humanize.naturaldelta(tstart - time.time())
                )
            )
        )
        embed.add_field(
            inline=False,
            name="Random",
            value="\U0001f1e6 {winrate_a} \U0001f1e7 {dmg_a} \U0001f1e8 {battles_a}".format(
                winrate_a=_("Winrate"), dmg_a=_("DpB"), battles_a=_("Battles")
            ),
        )

        embed.add_field(
            inline=False,
            name="Rating",
            value="\U0001f1fd {winrate_r} \U0001f1fe {dmg_r} \U0001f1ff {battles_r}".format(
                winrate_r=_("Winrate"), dmg_r=_("DpB"), battles_r=_("Battles")
            ),
        )
        embed.add_field(
            inline=False,
            name="Time",
            value="\U000023f2 {last} \U0001f4c5 {created}".format(
                last=_("Last battle"), created=_("Account created")
            ),
        )

        async def calc(what, style):
            out = []
            for v in guild_members.values():
                name = v["name"]
                if isinstance(v["wg_name"], dict):
                    wgname = v["wg_name"]["name"]
                else:
                    wgname = v["wg_name"]
                wr = v["wg_stats"][style].get(what, False)
                if v["wg_stats"][style].get(what, False):
                    out.append([wr, "{} ({})".format(name, wgname)])

            out = sorted(out, key=lambda x: (x[0]), reverse=True)

            embed.title = _("{} {} for `{}`:").format(
                what, style, ctx.message.guild.name
            )
            embed.description = "```{}```".format(
                tabulate(out, numalign="right", floatfmt=".2f")
            )
            return embed

        embed = await calc("winrate", "random")

        try:
            msg = await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )

        try:
            await msg.add_reaction("\U0001f1e6")
            await asyncio.sleep(0.5)
            await msg.add_reaction("\U0001f1e7")
            await msg.add_reaction("\U0001f1e8")
            await asyncio.sleep(0.5)
            await msg.add_reaction("\U0001f1fd")
            await msg.add_reaction("\U0001f1fe")
            await asyncio.sleep(0.5)
            await msg.add_reaction("\U0001f1ff")
            await msg.add_reaction("\U000023f2")
            await asyncio.sleep(0.5)
            await msg.add_reaction("\U0001f4c5")
        except discord.Forbidden:
            self.bot.logger.warning(
                "Please enable Add reactions permission for wotbot."
            )
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )

        await asyncio.sleep(0.3)

        def check(reaction, user):
            if msg.id == reaction.message.id:
                if user != msg.author:
                    return reaction, user

        while True:
            # await asyncio.sleep(10)
            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", check=check, timeout=10
                )
                if reaction.emoji == "\U0001f1e6":
                    embed = await calc("winrate", "random")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f1e7":
                    embed = await calc("dpb", "random")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f1e8":
                    embed = await calc("battles", "random")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f1fd":
                    embed = await calc("winrate", "rating")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f1fe":
                    embed = await calc("dpb", "rating")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f1ff":
                    embed = await calc("battles", "rating")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U000023f2":
                    embed = await calc("last", "random")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
                elif reaction.emoji == "\U0001f4c5":
                    embed = await calc("created", "random")
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        await msg.edit(content=None, embed=embed_short)
            except asyncio.TimeoutError:
                try:
                    await msg.remove_reaction("\U0001f1e6", msg.author)
                    await asyncio.sleep(0.2)
                    await msg.remove_reaction("\U0001f1e7", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U0001f1e8", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U0001f1fd", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U0001f1fe", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U0001f1ff", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U000023f2", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U0001f4c5", msg.author)
                    await asyncio.sleep(0.2)
                    await msg.add_reaction("\U00002611")
                except Exception as e:
                    self.bot.logger.info(e)
                break
            self.bot.logger.info("exit")

        print(humanize.naturaldelta(tstart - time.time()))
        self.bot.logger.info("end of guild wr")


def setup(bot):
    bot.add_cog(UserStats(bot))
